(function (Drupal, $) {

  'use strict';
  $(document).ready(function(){
	    var search_combine = $(".content-sidebar input#edit-combine-search").val();
	    $(".content-sidebar span#value_rechercher").text(search_combine);
    		var pgurl = window.location.pathname;
		     $(".responsive-menu li a").each(function(){
	          	if($(this).attr("href") == pgurl || $(this).attr("href") == '' ){
		          	$(this).addClass("active_res");
	         	}
		    });
		    var urllifestyle = window.location.pathname + window.location.search; 
		     $(".item-categorie-lifestyle li a").each(function(){
	          	if($(this).attr("href") === urllifestyle){
		          	$(this).parent().addClass("activated-thematic");
	         	}
		    });
		    if(urllifestyle === '/thematique/life-style?tags_id=all' || urllifestyle === '/ar/thematique/نمط-الحياة?tags_id=all') {
		    	$('li.item-categorie').addClass("activated-thematic");
		    }  

        $.ajax('https://covid19-bo.snrtnews.com/api/lastCases?api_token=uOVrcjEpLzIopeekeDjHWMRjOxvTerLrEdkfYkll', {
          dataType: 'json',
          success: function(data) {
            $('#nb_confirm').text(data.lastValidCases.confirmed);
            $('#nb_deces').text(data.lastValidCases.deaths);
            $('#nb_gueris').text(data.lastValidCases.recoveries);
            $('#last_update').text(data.lastValidCases.last_updated);
             // add caret up if nb_confirmed > 0
            if(data.lastValidCases.confirmed > 0)
              $('<i class="fa fa-caret-up fa-lg" id="fa-confirmed"></i>').insertAfter("#nb_confirm");
            
            // add caret up if nb_gueris > 0
            if(data.lastValidCases.recoveries > 0)
              $('<i class="fa fa-caret-up fa-lg" id="fa-gueris"></i>').insertAfter("#nb_gueris");

            // add caret up if nb_deces > 0
            if(data.lastValidCases.deaths > 0)
              $('<i class="fa fa-caret-up fa-lg" id="fa-deces"></i>').insertAfter("#nb_deces");
          }
        });
	});

  
  $("#menu-toggle").click(function () {
    $("#header #region-navigation-collapsible").css({"visibility": "visible", "opacity": "1", "transition": "all .35s linear .1s"});
    $("html body div#main").css({"padding-top": "0px", "margin-top": "-10px"});
    $("body header#header").css("position", "unset");
  });
  $("#block-closeresponsive").click(function () {
    $("body #header #region-navigation-collapsible").css({"visibility": "hidden", "opacity": "0", "transition": "all .35s linear .1s"});
    $("html body div#main").css({"padding-top": "106px", "margin-top": "0px"});
    $("body header#header").css("position", "fixed");
  });

  function createCookie(name,value,days) {
    if (days) {
      var date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
  }

  function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
  }
  function setMode(mode) {
    createCookie('view_mode', mode, 3560);
      //document.cookie = "mode="+ mode + "; expires=Fri, 31 Dec 9999 23:59:59 GMT";
      if(document.getElementById('botola-widget') != null) {
        document.getElementById('botola-widget').src = 'https://botola.snrt.ma/widget/?mode=' + mode;
      }
      if(document.getElementById('botola-widget-ar') != null) {
        document.getElementById('botola-widget-ar').src = 'https://botola.snrt.ma/widget/ar?mode=' + mode;
      }

      if(document.getElementById('covid-url') !=  null) {
        document.getElementById('covid-url').href = 'https://covid19.snrtnews.com/fr/?mode=' + mode;
      }

      if(document.getElementById('covid-url-ar') !=  null) {
        document.getElementById('covid-url-ar').href = 'https://covid19.snrtnews.com/ar/?mode=' + mode;
      }
  }
    //if (document.cookie.replace(/(?:(?:^|.*;\s*)mode\s*\=\s*([^;]*).*$)|^.*$/, "$1") == null) {
    var mode = readCookie('view_mode');
    if(mode == null) {
      console.log("Cookie not set");
      if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        console.log("Dark mode detected on browser");
        mode = 'dark';
        //document.cookie = "mode=dark; expires=Fri, 31 Dec 9999 23:59:59 GMT";
        // dark mode
      } else {
        console.log("Normal mode detected on browser");
        mode = 'normal';
        //document.cookie = "mode=normal; expires=Fri, 31 Dec 9999 23:59:59 GMT";
      }
      createCookie('view_mode', mode, 3560);
    }
    //var modeCookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)mode\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    // console.log("current mode " + mode);
    if(mode === "dark") {
        console.log("Cookie set : Dark mode detected on cookie");
      $("body").toggleClass("darkmode_style");
      setMode("dark");
      //document.getElementById('botola-widget').src = '//botola.snrt.ma/widget/?mode=dark';
      //document.getElementById('botola-widget-ar').src = '//botola.snrt.ma/widget/ar?mode=dark';
    }
  $(".darkmode").click(function () {
    $("body").toggleClass("darkmode_style");
    if($('.darkmode_style').length) {
        console.log("Dark mode choosed");
      setMode("dark");
      //document.cookie = "mode=dark; expires=Fri, 31 Dec 9999 23:59:59 GMT";
      //document.getElementById('botola-widget').src = '//botola.snrt.ma/widget/?mode=dark';
      //document.getElementById('botola-widget-ar').src = '//botola.snrt.ma/widget/ar?mode=dark';
    } else {
        console.log("Normal mode choosed");
      setMode("normal");
      //document.cookie = "mode=normal; expires=Fri, 31 Dec 9999 23:59:59 GMT";
      //document.getElementById('botola-widget').src = '//botola.snrt.ma/widget/';
      //document.getElementById('botola-widget-ar').src = '//botola.snrt.ma/widget/ar';
    }
  });

  $(".jt-holder").click(function () {
    var category = $(this).attr('data-categoriejt');
    var title = $(this).attr('data-title');
    var date_only = $(this).attr('data-difuse');
    var src = $(this).attr('data-src');
    if ($('body').hasClass("lang_ar")) {
       var date = "البث يوم " + date_only;
    } else {
       var date = "Diffusé le " + date_only;
    }
    var url = "https://www.youtube.com/embed/" + src;
    $(".iframe_jt").attr('src', url);
    $("#categori_popupjt").html(category);
    $("#title_popupjt").html(title);
    $("#date_popupjt").html(date);
    $("#main #block-popupjt").css("display", "block");
  });
  
  $("#cancel_button").click(function () {
    $("#main #block-popupjt").css("display", "none");
  });

  $("#feeds-block").click(function () {
    $(this).parent().addClass("active-click");
    $("#flashnews-block").parent().removeClass("active-click");
    $(".flash-news-block footer").css("display", "block");
    $(".flash-news-block .item-list").css("display", "none");
  });
  $("#flashnews-block").click(function () {
    $(this).parent().addClass("active-click");
    $("#feeds-block").parent().removeClass("active-click");
    $(".flash-news-block footer").css("display", "none");
    $(".flash-news-block .item-list").css("display", "block");
  });
  $("#your-question").click(function () {
    $(".section-vraifaux footer").css("display", "block");
  });
  $("#cancel-question").click(function () {
    $(".section-vraifaux footer").css("display", "none");
  });
  $("#suivant_info").click(function () {
    $(this).css("display", "none");
    $("#area-question").css("display", "none");
    $("#popup-question #info-person").css("display", "block");
    $("#popup-question .valider-question").css("display", "block");
  });

  $("#send_question").click(function () {
    var encoded_title = $("#pose_question").val();
    var title = encodeURIComponent(encoded_title);
    var encode_name = $("#username_question").val();
    var name = encodeURIComponent(encode_name);
    var email = $("#email_question").val();
    var lang = detect_lang();
    $.ajax({
      url: "/api/create_question/" + title + "/" + email + "/" + name + "/" + lang,
      type: "get",
      data: "", 
      success: function() {
        $(".section-vraifaux footer").css("display", "none");
      },
      error: function() {
        $("#error_label").val('Une erreur est survenu lors de lenvoie de la requette');
      }
    });
    return false;
  });
  function detect_lang() {
  if ($('body').hasClass("lang_ar")) {
       return "ar";
    } else {
       return "fr";
    }
  }
  function rtl_slick(){
  if ($('body').hasClass("lang_ar")) {
       return true;
    } else {
       return false;
    }
  }

$("#cancel_tts").click(function () {
  $(".text_speech").css({"visibility": "hidden", "opacity": "0", "transition": "all .45s ease .1s"});
  if(player != null) {
    player.pause();
  }
});
$("#play_tts").click(function () {
  $("body .text_speech").css({"visibility": "visible", "opacity": "1", "transition": "all .45s ease .1s"});
});  

  $('.news-slider .views-view-grid').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    rtl: rtl_slick(),
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 2,
          centerMode: false,
          slidesToScroll: 2
        }
      }
    ]
  });

  $('.item-list-direct ul').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    rtl: rtl_slick(),
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('ul.item-list-bricolage').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: false,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 2,
          centerMode: false,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 1
        }
      }
    ]
  });
  $('.front-page .item-list-agenda ul').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 1
        }
      }
    ]
  });
  $('.with-four .item-list-agenda ul').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('#block-views-block-7-7-block-block-2 .item-list-agenda ul').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.with-two .item-list-agenda ul').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.item-list-image ul').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    rtl: rtl_slick(),
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          arrows:true,
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('#meteo').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    rtl: rtl_slick(),
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          arrows:true,
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.item-list-vraifake ul').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.item-list-relation ul').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    rtl: rtl_slick(),
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 740,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1
        }
      }
    ]
  });
  // $( '.range-text' ).append( '<input type="range" min="10" max="30" value="16" id="fader" >' );
  // $("#fader").on("input",function () {
  //     $('.description-article').css("font-size", $(this).val() + "px");
  // });
  $(".buton1").click(function() {
    var fontSize = parseInt($('.description-article').css("font-size"));
    fontSize = fontSize - 1 + "px";
    $('.description-article').css({'font-size':fontSize});
  });
  $(".buton2").click(function() {
    var fontSize = parseInt($('.description-article').css("font-size"));
    fontSize = fontSize + 1 + "px";
    $('.description-article').css({'font-size':fontSize});
  });
  // $(".range-text").hide();
  $(".cancel_btn").click(function(){
    $(".direct").css("display", "none");
  });

  $(".category-tout").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("#block-views-block-7-7-block-block-3").css("display", "inline-block");
    $("#block-views-block-7-7-block-block-4").css("display", "inline-block");
    $("#block-views-block-7-7-block-block-5").css("display", "block");
    $("#block-views-block-7-7-block-block-6").css("display", "inline-block");
    $("#block-views-block-7-7-block-block-7").css("display", "inline-block");
    $("#block-views-block-7-7-block-block-8").css("display", "block");

    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });
  
  $(".category-exposition").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none");

    $("div#block-views-block-block-agenda-filter-block-1").css("display", "block");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });
  $(".category-theatre").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none"); 

    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "block");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });

  $(".category-concert").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none");
 
    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "block");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });

  $(".category-loisirs").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none");
 
    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "block");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });

  $(".category-danse").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none");
 
    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "block");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "none");
  });
  $(".category-humour").click(function () {
    $(".item-categorie").removeClass("activated-thematic");
    $(this).addClass("activated-thematic");
    $("div#block-views-block-7-7-block-block-3").css("display", "none");
    $("div#block-views-block-7-7-block-block-4").css("display", "none");
    $("div#block-views-block-7-7-block-block-5").css("display", "none");
    $("div#block-views-block-7-7-block-block-6").css("display", "none");
    $("div#block-views-block-7-7-block-block-7").css("display", "none");
    $("div#block-views-block-7-7-block-block-8").css("display", "none");
  
    $("div#block-views-block-block-agenda-filter-block-1").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-4").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-2").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-3").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-5").css("display", "none");
    $("div#block-views-block-block-agenda-filter-block-6").css("display", "block");
  });





})(Drupal, jQuery);
